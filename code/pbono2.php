<?php
abstract class BangunDatar
{
    public float $panjang = 10;
    public float $lebar = 5;

    abstract public function hitungLuas(): float;
    abstract public function hitungKeliling(): float;
}

class Persegi extends BangunDatar
{
    public float $sisi = 10;

    public function hitungLuas(): float
    {
        return pow($this->sisi, 2);
    }

    public function hitungKeliling(): float
    {
        return 4 * $this->sisi;
    }
}

class PersegiPanjang extends BangunDatar
{
    public function hitungLuas(): float
    {
        return $this->panjang * $this->lebar;
    }

    public function hitungKeliling(): float
    {
        return 2 * ($this->panjang + $this->lebar);
    }
}

class Segitiga extends BangunDatar
{
    public float $alas = 10;
    public float $tinggi = 5;

    public function hitungLuas(): float
    {
        return $this->alas * $this->tinggi / 2;
    }

    public function hitungKeliling(): float
    {
        return $this->alas + $this->tinggi + sqrt(pow($this->alas, 2) + pow($this->tinggi, 2));
    }
}

class Lingkaran extends BangunDatar
{
    public float $jariJari = 10;

    public function hitungLuas(): float
    {
        return pow(pi() * $this->jariJari, 2);
    }

    public function hitungKeliling(): float
    {
        return 2 * (pi() * $this->jariJari);
    }
}

class Trapesium extends BangunDatar
{
    public float $alasA = 6;
    public float $alasB = 7;
    public float $alasC = 10;
    public float $alasD = 5;
    public float $tinggi = 15;

    public function hitungLuas(): float
    {
        return $this->tinggi * ($this->alasA * $this->alasB) / 2;
    }

    public function hitungKeliling(): float
    {
        return $this->alasA + $this->alasB + $this->alasC + $this->alasD;
    }
}

class JajarGenjang extends BangunDatar
{
    public float $alas = 10;
    public float $tinggi = 5;
    public float $sisiLain = 7;

    public function hitungLuas(): float
    {
        return $this->alas * $this->tinggi;
    }

    public function hitungKeliling(): float
    {
        return 2 * ($this->alas + $this->sisiLain);
    }
}

class BelahKetupat extends BangunDatar
{
    public float $sisi = 10;
    public float $diagonal1 = 5;
    public float $diagonal2 = 7;

    public function hitungLuas(): float
    {
        return 4 * $this->sisi;
    }

    public function hitungKeliling(): float
    {
        return $this->diagonal1 * $this->diagonal2 / 2;
    }
}

$square = new Persegi();
$rectangle = new PersegiPanjang();
$triangle = new Segitiga();
$circle = new Lingkaran();
$trapezoid = new Trapesium();
$parallelogram = new Persegi();
$rhombus = new BelahKetupat();

echo "Luas Persegi " . $square->hitungLuas() . "<br>";
echo "Keliling Persegi " . $square->hitungKeliling() . "<br>";
echo "<br>";
echo "Luas Persegi Panjang " . $rectangle->hitungLuas() . "<br>";
echo "Keliling Persegi Panjang " . $rectangle->hitungKeliling() . "<br>";
echo "<br>";
echo "Luas Segitiga " . $triangle->hitungLuas() . "<br>";
echo "Keliling Segitiga " . $triangle->hitungKeliling() . "<br>";
echo "<br>";
echo "Luas Lingkaran " . $circle->hitungLuas() . "<br>";
echo "Keliling Lingkaran " . $circle->hitungKeliling()  . "<br>";
echo "<br>";
echo "Luas Trapesium " . $trapezoid->hitungLuas() . "<br>";
echo "Keliling Trapesium " . $trapezoid->hitungKeliling() . "<br>";
echo "<br>";
echo "Luas Jajar Genjang " . $parallelogram->hitungLuas() . "<br>";
echo "Keliling Jajar Genjang " . $parallelogram->hitungKeliling() . "<br>";
echo "<br>";
echo "Luas Belah Ketupat " . $rhombus->hitungLuas() . "<br>";
echo "Keliling Belah Ketupat " . $rhombus->hitungKeliling() . "<br>";
