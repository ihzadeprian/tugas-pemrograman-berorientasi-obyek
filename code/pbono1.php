<?php
class LifeForm
{
    public string $numberOfLegs;
    public bool $canFly;
    public string $foodClassification;
    public bool $canSwim;
    public string $respirationOrgan;

    public function eat(): string
    {
        return "This Life Form is Classified as " . $this->foodClassification . " Eaters";
    }

    public function run(): string
    {
        return "This Life Form has " . $this->numberOfLegs . " Leg/Legs";
    }

    public function swim(): string
    {
        if ($this->canSwim == true) {
            return "This Life Form can Swimming";
        } else {
            return "This Life Form can not Swimming";
        }
    }

    public function fly(): string
    {
        if ($this->canFly == true) {
            return "This Life Form can Flying";
        } else {
            return "This Life Form can not Flying";
        }
    }

    public function cry(): string
    {
        return "This Life Form is Crying";
    }
}

class CatFish extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 0;
        $this->respirationOrgan = "Gills";
        $this->foodClassification = "Omnivore";
        $this->canRun = false;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class BettaFish extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 0;
        $this->respirationOrgan = "Gills";
        $this->foodClassification = "Omnivore";
        $this->canRun = false;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Crocodile extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 4;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carnivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Alligator extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 4;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carnivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Lizard extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 4;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Omnivore";
        $this->canRun = true;
        $this->canSwim = false;
        $this->canFly = false;
    }
}

class Snake extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 0;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carbivore";
        $this->canRun = false;
        $this->canSwim = false;
        $this->canFly = false;
    }
}

class Swan extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 2;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Herbivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = true;
    }
}

class Chicken extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 2;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Herbivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Swallow extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 2;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Herbivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = true;
    }
}

class Duck extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 2;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Herbivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Human extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 2;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Omnivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Whale extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 0;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carnivore";
        $this->canRun = false;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Dolphine extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 0;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carnivore";
        $this->canRun = false;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

class Bat extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 2;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carnivore";
        $this->canRun = false;
        $this->canSwim = false;
        $this->canFly = true;
    }
}
class Tiger extends LifeForm
{
    public function __construct()
    {
        $this->numberOfLegs = 4;
        $this->respirationOrgan = "Lungs";
        $this->foodClassification = "Carnivore";
        $this->canRun = true;
        $this->canSwim = true;
        $this->canFly = false;
    }
}

$budi = new Human();

echo $budi->eat();
echo "<br>";
echo $budi->run();
echo "<br>";
echo $budi->swim();
echo "<br>";
echo $budi->fly();
echo "<br>";
echo $budi->cry();
echo "<br>";
